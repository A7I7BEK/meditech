

/*__________ LOAD Full Page +++ __________*/
var slideMore = false;

/* Full Page +++ */
$(document).ready(function() {
	$('.fullpage').fullpage({
		anchors: ['page1', 'page2', 'page3', 'page4', 'page5', 'page6'],
		menu: '.page-indicator, .nav-basic-menu',
		normalScrollElements: '.catalog-box',
		scrollingSpeed: 1000,
		loopTop: true,
		loopBottom: true,
		css3: false,
		afterLoad: function(anchorLink, index){
			
			if (index === 3 || index === 1)
			{
				$('.frame-box').removeClass('dark');
			}
			else
			{
				$('.frame-box').addClass('dark');
			}

			// -------------------------------------------

			if (index === 3)
			{
				$('.frame.right').addClass('shadow-light');
			}
			else
			{
				$('.frame.right').removeClass('shadow-light');
			}

			// -------------------------------------------

			if (index === 4)
			{
				$('.frame.right').addClass('shadow-dark');
			}
			else
			{
				$('.frame.right').removeClass('shadow-dark');
			}

			// -------------------------------------------

			if (index === 1)
			{
				$('.arrow-box').removeClass('hidden');
			}
			else
			{
				$('.arrow-box').addClass('hidden');
			}
			

		},
		afterSlideLoad: function( anchorLink, index, slideAnchor, slideIndex){
			if (index === 2 & slideIndex === 1)
			{
				$('.catalog-box').addClass('narrow');
				$('.page-indicator-box').removeClass('active');
				$('.return').addClass('active');
				$('.frame.right').addClass('shadow-dark');

				slideMore = true;
			}
			else
			{
				$('.catalog-box').removeClass('narrow');
				$('.page-indicator-box').addClass('active');
				$('.return').removeClass('active');
				$('.frame.right').removeClass('shadow-dark');

				slideMore = false;
			}
		},
		onLeave: function(index, nextIndex, direction){

			if(slideMore){
				$.fn.fullpage.moveTo(2, 0);
				// return false;
			}
		}


	});
});
/* Full Page --- */


/* Arrows +++ */
$('.arrow.left').click(function () {
	$.fn.fullpage.moveSlideLeft();
});

$('.arrow.right').click(function () {
	$.fn.fullpage.moveSlideRight();
});
/* Arrows --- */



/* Equipment +++ */

// Go to Information about Equipments
$('.equipment').click(function () {
	$.fn.fullpage.moveTo(2, 1);

	return false;
});


// Return Equipments
$('.return').click(function () {
	$.fn.fullpage.moveTo(2, 0);

	return false;
});
/* Equipment --- */

/*__________ LOAD Full Page --- __________*/




/*__________ Scroll Bar +++ __________*/
$('.about-article-text').mCustomScrollbar({
	callbacks:{
		whileScrolling: function(){

			var overview = parseInt($('.about-article-text .mCSB_container').css('top'));
			var achievement = $('.about-achievement').position().top * (-1);
			var license = $('.about-license').position().top * (-1);

			var overviewHeight = $('.about-article-text .mCSB_container').height() * (-1);
			var viewportHeight = $('.about-article-text .mCustomScrollBox').height() * (-1);
			var scrollBottom = overviewHeight - viewportHeight;

			if (overview < license + 150 || overview < scrollBottom + 10)
			{
				$('.about-heading').eq(2).addClass('active').siblings().removeClass('active');
				$('.about-img').eq(2).addClass('active').siblings().removeClass('active');
			}
			else if (overview < achievement + 150)
			{
				$('.about-heading').eq(1).addClass('active').siblings().removeClass('active');
				$('.about-img').eq(1).addClass('active').siblings().removeClass('active');
			}
			else
			{
				$('.about-heading').eq(0).addClass('active').siblings().removeClass('active');
				$('.about-img').eq(0).addClass('active').siblings().removeClass('active');
			}

		}
	}
});
/*__________ Scroll Bar --- __________*/



