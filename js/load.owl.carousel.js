$(document).ready(function() {

	$(".svc-item-list").owlCarousel({
		itemsCustom: [
			[0, 1],
			[480, 2],
			[730, 3],
			[800, 4],
			[992, 3],
			[1320, 4],
			[1650, 5]
		],
		navigation: true,
		pagination: false
	});

});