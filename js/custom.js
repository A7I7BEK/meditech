
/*__________ Search +++ __________*/
$('.search-text, .search-input-close').click(function () {
	if ($('.search-text').hasClass('close'))
	{
		$('.search-text').removeClass('close');
		$('.search-input-box').removeClass('open');
	}
	else
	{
		$('.search-text').addClass('close');
		$('.search-input-box').addClass('open');
		$('.search-input-box > input').focus();
	}
});
/*__________ Search --- __________*/




/*__________ Basic Navigation +++ __________*/
$('.nav-basic-btn-open, .nav-basic-btn-close, .nav-basic-bg, .nav-basic-menu > li > a').click(function () {
	if ($('.nav-basic').hasClass('open'))
	{
		$('.nav-basic').removeClass('open');
		$('.nav-basic-bg').removeClass('active');
	}
	else
	{
		$('.nav-basic').addClass('open');
		$('.nav-basic-bg').addClass('active');
	}
});
/*__________ Basic Navigation --- __________*/



/* CHANGE +++ */
/*__________ Telephone Call Modal +++ __________*/
$('.tel-call > a, .tel-call-modal-close, .tel-call-modal-bg').click(function () {
	if ($('.tel-call-modal-box').hasClass('active'))
	{
		$('.tel-call-modal-box').removeClass('active');
		$('.tel-call-modal-bg').removeClass('active');
	}
	else
	{
		$('.tel-call-modal-box').addClass('active');
		$('.tel-call-modal-bg').addClass('active');
	}

	return false;
});


$('.tel-call-modal-submit > button').click(function () {
	if ($('.tel-call-modal-app').hasClass('active'))
	{
		$('.tel-call-modal-app').removeClass('active');
		$('.tel-call-modal-received').addClass('active');
	}
	else
	{
		$('.tel-call-modal-app').addClass('active');
		$('.tel-call-modal-received').removeClass('active');
	}
});
/*__________ Telephone Call Modal --- __________*/
/* CHANGE --- */




/*__________ Catalog +++ __________*/
$('.catalog-btn-open, .catalog-btn-close, .catalog-box-bg').click(function () {
	if ($('.catalog-box').hasClass('open'))
	{
		$('.catalog-box').removeClass('open');
		$('.catalog-box-bg').removeClass('active');
	}
	else
	{
		$('.catalog-box').addClass('open');
		$('.catalog-box-bg').addClass('active');
	}
});


$('.catalog-btn-enlarge').click(function () {
	if ($('.catalog-box').hasClass('large'))
	{
		$('.catalog-box').removeClass('large');
		$(this).removeClass('active');
	}
	else
	{
		$('.catalog-box').addClass('large');
		$(this).addClass('active');
	}
});
/*__________ Catalog +++ __________*/




/*__________ Service +++ __________*/
$('.service-list > li > a').mouseover(function () {

	$('.service-list > li').removeClass('active');
	$(this).parent().addClass('active');

	// Show Parallel element
	$('.service-img > img').removeClass('active');
	$('.service-img > img').eq($(this).parent().index()).addClass('active');

});
/*__________ Service --- __________*/




/*__________ News +++ __________*/
$('.news-list > li > a').click(function () {

	$('.news-list > li').removeClass('active');
	$(this).parent().addClass('active');

	// Show Parallel element
	$('.news-article').removeClass('active');
	$('.news-article').eq($(this).parent().index()).addClass('active');


	// Close News Box in Mobile View
	$('.news-btn').removeClass('active');
	$('.news-box').removeClass('active');

	return false;
});


$('.news-btn').click(function () {
	if ($(this).hasClass('active'))
	{
		$(this).removeClass('active');
		$(this).parent('.news-box').removeClass('active');
	}
	else
	{
		$(this).addClass('active');
		$(this).parent('.news-box').addClass('active');
	}
});
/*__________ News +++ __________*/




/*__________ Service Item +++ __________*/
$('.svc-item-list .item > a').click(function () {

	$('.svc-item-list .item > a').removeClass('active');
	$(this).addClass('active');

	var counter = 0;
	for (var i = 0; i < $('.svc-item-list .item > a').length; i++) {

		if ($('.svc-item-list .item > a').eq(i).hasClass('active'))
		{
			break;
		}

		counter++;
	}

	// Show Parallel element
	$('.svc-item-article').removeClass('active');
	$('.svc-item-article').eq(counter).addClass('active');

	return false;
});

/*__________ Service Item --- __________*/




/*__________ Details +++ __________*/
$('.details-hide-btn').click(function () {
	if ($(this).hasClass('hidden'))
	{
		$(this).removeClass('hidden');
		$('.details-box').removeClass('hidden');
		$('.details-number').removeClass('hidden');
	}
	else
	{
		$(this).addClass('hidden');
		$('.details-box').addClass('hidden');
		$('.details-number').addClass('hidden');
	}
});



$('.details-input > input').click(function () {
	$('.details-input').removeClass('active');
	$(this).parent().addClass('active');
});
$(document).click(function (e){
	if ($(e.target).is(".details-input > input") === false)
	{
		$('.details-input').removeClass('active');
	}
});
/*__________ Details --- __________*/




/*__________ Language +++ __________*/
$('.lang').click(function () {
	$('.lang-dropdown').toggleClass('active');

	var lang_active = $('.lang.active').text();

	$('.lang.active').text($(this).text());

	$(this).text(lang_active);

	return false;
});
/*__________ Language --- __________*/


