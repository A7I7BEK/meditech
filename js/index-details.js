

$(document).ready(function() {
	$('.fullpage').fullpage({
		normalScrollElements: '.catalog-box, .tiny-scroll',
		scrollingSpeed: 1000,
		css3: false,
		afterSlideLoad: function(anchorLink, index, slideAnchor, slideIndex){

			if (slideIndex === 2)
			{
				$('.frame.right').removeClass('shadow-dark');
			}
			else
			{
				$('.frame.right').addClass('shadow-dark');
			}
		}

	});
});


// Application button in the Base page
$('.details-btn-application').click(function () {
	$.fn.fullpage.moveTo(1, 0);

	$('.catalog-box').removeClass('narrow');

	$('.return').removeClass('active');	// CHANGE
	$('.return.app-more').addClass('active');	// CHANGE

	return false;
});


// More button in the Base page
$('.details-btn-more').click(function () {
	$.fn.fullpage.moveTo(1, 2);

	$('.catalog-box').removeClass('narrow');

	$('.return').removeClass('active');	// CHANGE
	$('.return.app-more').addClass('active');	// CHANGE

	return false;
});


// Cencel and Return buttons in the Application and More pages
$('.details-btn-cancel, .return.app-more').click(function () {
	$.fn.fullpage.moveTo(1, 1);

	$('.catalog-box').addClass('narrow');

	$('.return').removeClass('active');	// CHANGE
	$('.return.base').addClass('active');	// CHANGE

	return false;
});